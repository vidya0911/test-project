from django.http import HttpResponse , HttpRequest
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.contrib.auth.forms import AuthenticationForm
#from django.template.loader import get_template
#from django.template import Context
from django.shortcuts import render_to_response
from django.shortcuts import render
from orgdata.models import Employee , Manager , Deleted
from django.core import serializers
from django.template import RequestContext
from django.utils import simplejson
from django.core.serializers.json import DjangoJSONEncoder
import json
import simplejson
from django.template.loader import get_template
#from onlinetest.models import Test, Option , Correct_Answer , Test_Score , Subject , Option ,Question
 # Import Test model in  defined in onlinetest(app).models i.e: we need it in this view
from django.db.models import Q

def search(request):
    error = False
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:
            error = True
        else:
            employee = Employee.objects.filter(Q(first_name__istartswith=q) | Q(last_name__istartswith=q))
            return render(request, 'search_results.html',
                {'employees': employee, 'query': q ,'error':error })
    return render(request, 'search_form.html',
        {'error': error})

def search_deleted(request):
    error = False
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:
            error = True
        else:
            employee = Deleted.objects.filter(Q(first_name__istartswith=q) | Q(last_name__istartswith=q))
            return render(request, 'search_results_deleted.html',
                {'employees': employee, 'query': q ,'error':error })
    return render(request, 'search_form_deleted.html',
        {'error': error})
    
    
def search_ex_emp(request , emp_id):
    ex_emp = Deleted.objects.get(id = emp_id)
    #subordinates = find_sub_ordinates(request , emp_id)
    #org_heirachy = find_heirachy(request,emp_id)
    return render_to_response('ex_emp_page.html', {'employee': ex_emp  } ,context_instance=RequestContext(request))





def home(request):
    return render(request,'home.html')

def delete_employee(request,emp_id):
    employee = Employee.objects.get(id = emp_id)
    deleted_employee= Deleted()
    deleted_employee.first_name = employee.first_name
    deleted_employee.last_name = employee.last_name
    deleted_employee.manager = employee.manager
    deleted_employee.business_unit = employee.business_unit
    deleted_employee.designation = employee.designation
    deleted_employee.joining_date = employee.joining_date
    deleted_employee.leaving_date = employee.last_modified
    deleted_employee.save()
    employee.delete()
    return render_to_response('emp_page.html', {'employee': deleted_employee ,'message': 'employee deleted successfully'} ,context_instance=RequestContext(request))

def confirm_delete(request,emp_id):
    employee = Employee.objects.get(id = emp_id) # this is  to display the employee details , 
    emp_manager_id = employee.manager_id
    subordinates = find_sub_ordinates(request , emp_id) # isplay subordinates , employee with same manager are subordinates except the employe
    org_heirachy = find_heirachy(request,emp_id)
    message = 'Are you Sure , This employee has left the company'
    return render_to_response('emp_page.html', {'employee': employee ,'subordinates':subordinates , 'org_heirachy':org_heirachy,'message':message } ,context_instance=RequestContext(request))


def check_emp(request ,emp_id):
    employee = Employee.objects.get(id = emp_id)
    if employee.manager_id is None:
        return render(request , 'del_ceo.html')
    else:
        manager_count =  Manager.objects.filter(manager_id = employee.id).count()
        if manager_count == 0:
           return delete_employee(request , emp_id)
        else:
           return del_manager(request,emp_id)
                    
def emp_page(request,emp_id):
    employee = Employee.objects.get(id = emp_id)
    subordinates = find_sub_ordinates(request , emp_id)
    org_heirachy = find_heirachy(request,emp_id)

    return render_to_response('emp_page.html', {'employee': employee ,'subordinates':subordinates , 'org_heirachy':org_heirachy } ,context_instance=RequestContext(request))

def del_manager(request , ex_emp_id):
    ex_employee = Employee.objects.get( id = ex_emp_id)
    ex_emp_manager =  ex_employee.manager
    import pdb
    pdb.set_trace()
    subordinate_manager = Manager.objects.get(manager = ex_employee)
    ex_emp_subordinates = Employee.objects.filter(manager = subordinate_manager)
    for ex_emp_subordinate in ex_emp_subordinates:
        ex_emp_subordinate.manager = ex_emp_manager
        ex_emp_subordinate.save()
    return delete_employee(request , ex_emp_id)

def find_sub_ordinates(request , emp_id):
    employee =  Employee.objects.get(id = emp_id )
    sub_ord_list_1 = []
    emp_subordinate = Manager.objects.filter(manager = employee)
    if emp_subordinate.count() > 0:
        #import pdb 
        #pdb.set_trace()
        sub_ord_lists = Employee.objects.filter(manager = emp_subordinate[0])
        for sub_ordinate in sub_ord_lists:
            sub_ord_list_1.append(sub_ordinate)
    return sub_ord_list_1


def find_heirachy(request,emp_id):
    sub_ordinate_manager= Employee.objects.get(id = emp_id)
    org_heirachy = [sub_ordinate_manager]
    while (sub_ordinate_manager.manager_id is not None):
        manager = Manager.objects.get(id = sub_ordinate_manager.manager_id)
        sub_ordinate_manager = Employee.objects.get(id = manager.manager_id)
        org_heirachy.append(sub_ordinate_manager)
    return org_heirachy

def dump_json(request , emp_id):
    emp = Employee.objects.get(id = emp_id)
    emp_as_dict = {
            'id': emp.id,
            'F_Name': emp.first_name ,
            'L_Name': emp.last_name ,
            'Ph.no:': emp.phone_number ,
            'Manager':str(emp.manager),
            'Business Unit':emp.business_unit.business_unit,
            'Designation': emp.designation.designation ,
            'Joining Date': str(emp.joining_date) ,
            'Last_Modified': str(emp.last_modified) 
            }
    json_obj = json.dumps(emp_as_dict)
    return render(request, 'dump_json.html',{'json_obj': json_obj} )

def logout_view(request):
    auth.logout(request)
    return HttpResponseRedirect('/')

def toJSON(self):
    return simplejson.dumps(dict([(attr, getattr(self, attr)) for attr in [f.name for f in self._meta.fields]]))
