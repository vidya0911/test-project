from django.conf.urls.defaults import *
from test_project import views
from views import home , search , emp_page , dump_json ,logout_view , delete_employee , search_deleted , confirm_delete , check_emp , search_ex_emp
from django.contrib import admin
from django.contrib.auth import login
admin.autodiscover()

urlpatterns = patterns('',
    ('^$', search),
    ('^home/$', home),
    ('^search/$', search),
    ('^search_deleted/$', search_deleted),
    ('^search_deleted/(\d{1,2})/$', search_ex_emp),
    ('^confirm_delete/(\d{1,2})/$', confirm_delete),      
    ('^emp_details/(\d{1,2})/$',emp_page),
    ('^check_emp/(\d{1,2})/$',check_emp),
    ('^search/(\d{1,2})/(\d{1,2})/$', delete_employee),
    ('^json/(\d{1,2})/$',dump_json),
    ('^logout/$',logout_view),
    ('^admin/', include(admin.site.urls)),
)

urlpatterns  += patterns('django.contrib.auth.views',
        ('^login/$', 'login', {'template_name': 'login.html'}),
 )
