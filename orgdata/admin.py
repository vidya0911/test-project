from django.contrib import admin
from orgdata.models import Manager , Business_Unit , Employee , Deleted , Designation


admin.site.register(Business_Unit)
admin.site.register(Designation)
admin.site.register(Employee)
admin.site.register(Manager)
admin.site.register(Deleted)

