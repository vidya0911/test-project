from django.db import models
from django.utils import timezone
class Designation(models.Model):
    designation = models.CharField(max_length=50)
    def __unicode__(self):
        return self.designation

class Manager(models.Model):
    
    manager = models.ForeignKey('Employee' , null = True ,blank = True, related_name = '%(class)s_manager') 

    def __unicode__(self):
        #return "First Name : {0} , Last Name : {1}".format(self.manager.first_name , self.manager.last_name )
        return u'{0}'.format(self.manager)


class Business_Unit(models.Model):
    business_unit = models.CharField(max_length = 1500)
  
    def __unicode__(self):
        return self.business_unit 

class Employee(models.Model):
    first_name = models.CharField(max_length = 20)
    last_name = models.CharField(max_length = 20)
    phone_number = models.CharField(max_length = 20)
    # photo = models.ImageField('Employee Photo', upload_to  = '/media')
    manager = models.ForeignKey('Manager' ,null = True, blank = True, related_name = '%(class)s_manager')
    business_unit = models.ForeignKey(Business_Unit)
    designation = models.ForeignKey(Designation)
    joining_date = models.DateField(auto_now_add = True)
    last_modified = models.DateField(auto_now = True)
   
    def __unicode__(self):
        return u"First Name : {0} , Last Name : {1}".format(self.first_name,self.last_name )

class Deleted(models.Model):
    first_name = models.CharField(max_length = 20)
    last_name = models.CharField(max_length = 20)
    phone_number = models.CharField(max_length = 20)
    #photo = models.ImageField('Employee Photo', upload_to='/media')
    manager = models.ForeignKey(Manager)
    business_unit = models.ForeignKey(Business_Unit)
    designation = models.ForeignKey(Designation)
    joining_date = models.DateField()
    leaving_date = models.DateField()


    def __unicode__(self):
        return u"First Name : {0} , Last_Name : {1}".format(self.first_name,self.last_name)






   



