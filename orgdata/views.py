# Create your views here.

from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from django.shortcuts import render
from orgdata.models import Employee


def search(request):
    error = False
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:
            error = True
        else:
            employee = Employee.objects.filter(first_name__icontains=q)
            return render(request, 'search_results.html',
                {'employee': employee, 'query': q ,'error':error })
    return render(request, 'search_form.html',
        {'error': error})